﻿namespace TaxiFareCal
{
    partial class MainForm
    {
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.Label vehicleTypeLabel;
        private System.Windows.Forms.Label baseFarePriceLabel;
        private System.Windows.Forms.Label baseFareDistanceLabel;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.TextBox surnameTxt;
        private System.Windows.Forms.TextBox emailTxt;
        private System.Windows.Forms.TextBox vehicleTypeTxt;
        private System.Windows.Forms.TextBox baseFarePriceTxt;
        private System.Windows.Forms.TextBox baseFareDistanceTxt;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridView driverGridView;
        private System.Windows.Forms.DataGridView fareGridView;
        private System.Windows.Forms.Button btnCalculate;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.vehicleTypeLabel = new System.Windows.Forms.Label();
            this.baseFarePriceLabel = new System.Windows.Forms.Label();
            this.baseFareDistanceLabel = new System.Windows.Forms.Label();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.surnameTxt = new System.Windows.Forms.TextBox();
            this.emailTxt = new System.Windows.Forms.TextBox();
            this.vehicleTypeTxt = new System.Windows.Forms.TextBox();
            this.baseFarePriceTxt = new System.Windows.Forms.TextBox();
            this.baseFareDistanceTxt = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.driverGridView = new System.Windows.Forms.DataGridView();
            this.fareGridView = new System.Windows.Forms.DataGridView();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClearHistory = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.driverGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fareGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(10, 14);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(39, 15);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name";
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(10, 42);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(54, 15);
            this.surnameLabel.TabIndex = 1;
            this.surnameLabel.Text = "Surname";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Location = new System.Drawing.Point(10, 70);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(36, 15);
            this.emailLabel.TabIndex = 2;
            this.emailLabel.Text = "Email";
            // 
            // vehicleTypeLabel
            // 
            this.vehicleTypeLabel.AutoSize = true;
            this.vehicleTypeLabel.Location = new System.Drawing.Point(10, 98);
            this.vehicleTypeLabel.Name = "vehicleTypeLabel";
            this.vehicleTypeLabel.Size = new System.Drawing.Size(71, 15);
            this.vehicleTypeLabel.TabIndex = 3;
            this.vehicleTypeLabel.Text = "Vehicle Type";
            // 
            // baseFarePriceLabel
            // 
            this.baseFarePriceLabel.AutoSize = true;
            this.baseFarePriceLabel.Location = new System.Drawing.Point(10, 127);
            this.baseFarePriceLabel.Name = "baseFarePriceLabel";
            this.baseFarePriceLabel.Size = new System.Drawing.Size(85, 15);
            this.baseFarePriceLabel.TabIndex = 4;
            this.baseFarePriceLabel.Text = "Base Fare Price";
            // 
            // baseFareDistanceLabel
            // 
            this.baseFareDistanceLabel.AutoSize = true;
            this.baseFareDistanceLabel.Location = new System.Drawing.Point(10, 155);
            this.baseFareDistanceLabel.Name = "baseFareDistanceLabel";
            this.baseFareDistanceLabel.Size = new System.Drawing.Size(104, 15);
            this.baseFareDistanceLabel.TabIndex = 5;
            this.baseFareDistanceLabel.Text = "Base Fare Distance";
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(131, 11);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(176, 23);
            this.nameTxt.TabIndex = 6;
            // 
            // surnameTxt
            // 
            this.surnameTxt.Location = new System.Drawing.Point(131, 39);
            this.surnameTxt.Name = "surnameTxt";
            this.surnameTxt.Size = new System.Drawing.Size(176, 23);
            this.surnameTxt.TabIndex = 7;
            // 
            // emailTxt
            // 
            this.emailTxt.Location = new System.Drawing.Point(131, 68);
            this.emailTxt.Name = "emailTxt";
            this.emailTxt.Size = new System.Drawing.Size(176, 23);
            this.emailTxt.TabIndex = 8;
            // 
            // vehicleTypeTxt
            // 
            this.vehicleTypeTxt.Location = new System.Drawing.Point(131, 96);
            this.vehicleTypeTxt.Name = "vehicleTypeTxt";
            this.vehicleTypeTxt.Size = new System.Drawing.Size(176, 23);
            this.vehicleTypeTxt.TabIndex = 9;
            // 
            // baseFarePriceTxt
            // 
            this.baseFarePriceTxt.Location = new System.Drawing.Point(131, 124);
            this.baseFarePriceTxt.Name = "baseFarePriceTxt";
            this.baseFarePriceTxt.Size = new System.Drawing.Size(176, 23);
            this.baseFarePriceTxt.TabIndex = 10;
            // 
            // baseFareDistanceTxt
            // 
            this.baseFareDistanceTxt.Location = new System.Drawing.Point(131, 152);
            this.baseFareDistanceTxt.Name = "baseFareDistanceTxt";
            this.baseFareDistanceTxt.Size = new System.Drawing.Size(176, 23);
            this.baseFareDistanceTxt.TabIndex = 11;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(10, 188);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(66, 28);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(84, 188);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(66, 28);
            this.btnDel.TabIndex = 13;
            this.btnDel.Text = "Delete";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(160, 188);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(66, 28);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // driverGridView
            // 
            this.driverGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.driverGridView.Location = new System.Drawing.Point(324, 11);
            this.driverGridView.Name = "driverGridView";
            this.driverGridView.RowHeadersWidth = 51;
            this.driverGridView.RowTemplate.Height = 24;
            this.driverGridView.Size = new System.Drawing.Size(481, 164);
            this.driverGridView.TabIndex = 15;
            this.driverGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.driverGridView_CellClick);
            // 
            // fareGridView
            // 
            this.fareGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fareGridView.Location = new System.Drawing.Point(324, 188);
            this.fareGridView.Name = "fareGridView";
            this.fareGridView.RowHeadersWidth = 51;
            this.fareGridView.RowTemplate.Height = 24;
            this.fareGridView.Size = new System.Drawing.Size(481, 214);
            this.fareGridView.TabIndex = 16;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(43, 239);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(107, 38);
            this.btnCalculate.TabIndex = 17;
            this.btnCalculate.Text = "Calculate Fare";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(241, 188);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(66, 28);
            this.btnClear.TabIndex = 18;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClearHistory
            // 
            this.btnClearHistory.Location = new System.Drawing.Point(160, 239);
            this.btnClearHistory.Name = "btnClearHistory";
            this.btnClearHistory.Size = new System.Drawing.Size(107, 38);
            this.btnClearHistory.TabIndex = 19;
            this.btnClearHistory.Text = "Clear History";
            this.btnClearHistory.UseVisualStyleBackColor = true;
            this.btnClearHistory.Click += new System.EventHandler(this.btnClearHistory_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 414);
            this.Controls.Add(this.btnClearHistory);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.fareGridView);
            this.Controls.Add(this.driverGridView);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.baseFareDistanceTxt);
            this.Controls.Add(this.baseFarePriceTxt);
            this.Controls.Add(this.vehicleTypeTxt);
            this.Controls.Add(this.emailTxt);
            this.Controls.Add(this.surnameTxt);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.baseFareDistanceLabel);
            this.Controls.Add(this.baseFarePriceLabel);
            this.Controls.Add(this.vehicleTypeLabel);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.surnameLabel);
            this.Controls.Add(this.nameLabel);
            this.MaximumSize = new System.Drawing.Size(832, 453);
            this.MinimumSize = new System.Drawing.Size(832, 453);
            this.Name = "MainForm";
            this.Text = "Taxi Fare Calculator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.driverGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fareGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private Button btnClear;
        private Button btnClearHistory;
    }
}

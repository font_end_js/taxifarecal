﻿using Newtonsoft.Json;

namespace TaxiFareCal
{
    public partial class MainForm : Form
    {
        private List<Driver> drivers;
        private List<FareInfo> fareHistory;
        private string driversFilePath = "driversinfo.json";
        private string fareHistoryPath = "farehistory.json";

        public MainForm()
        {
            InitializeComponent();
            drivers = new List<Driver>();
            fareHistory = new List<FareInfo>();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //Load Data GridView
            RefreshGridView();

        }

        private void driverGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = driverGridView.Rows[e.RowIndex];

                string name = row.Cells["Name"].Value.ToString();
                string surName = row.Cells["SurName"].Value.ToString();
                string email = row.Cells["Email"].Value.ToString();
                string vehicleType = row.Cells["VehicleType"].Value.ToString();
                string baseFarePrice = row.Cells["BaseFarePrice"].Value.ToString();
                string baseFareDistance = row.Cells["BaseFareDistance"].Value.ToString();

                nameTxt.Text = name;
                surnameTxt.Text = surName;
                emailTxt.Text = email;
                vehicleTypeTxt.Text = vehicleType;
                baseFarePriceTxt.Text = baseFarePrice;
                baseFareDistanceTxt.Text = baseFareDistance;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (driverGridView.SelectedRows.Count > 0)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete the driver?", "Confirmation", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    int slcRowIndex = driverGridView.SelectedRows[0].Index;
                    drivers.RemoveAt(slcRowIndex);
                    SaveDriverDataToJson(drivers, driversFilePath);
                    RefreshGridView();
                    ClearDriverInput();
                }
            }
            else
            {
                MessageBox.Show("Please select a driver first.", "Message");
            }
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearDriverInput();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (driverGridView.SelectedRows.Count > 0)
            {
                string name = nameTxt.Text.Trim();
                string surName = surnameTxt.Text.Trim();
                string email = emailTxt.Text.Trim();
                string vehicleType = vehicleTypeTxt.Text.Trim();
                int baseFarePrice;
                int baseFareDistance;

                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(surName) || string.IsNullOrEmpty(email) ||
                    string.IsNullOrEmpty(vehicleType) || !int.TryParse(baseFarePriceTxt.Text, out baseFarePrice) ||
                    !int.TryParse(baseFareDistanceTxt.Text, out baseFareDistance))
                {
                    MessageBox.Show("Please enter valid data for all fields!", "Error");
                    return;
                }

                int isExits = drivers.Where(r => r.Email == email).Count();
                {
                    if (isExits > 0)
                    {
                        int slcRowIndex = driverGridView.SelectedRows[0].Index;
                        Driver slcDriver = drivers[slcRowIndex];

                        slcDriver.Name = nameTxt.Text.Trim();
                        slcDriver.Surname = surnameTxt.Text.Trim();
                        slcDriver.VehicleType = vehicleTypeTxt.Text.Trim();
                        slcDriver.BaseFarePrice = int.Parse(baseFarePriceTxt.Text);
                        slcDriver.BaseFareDistance = int.Parse(baseFareDistanceTxt.Text);

                        DialogResult result = MessageBox.Show("Are you sure you want to update the driver?", "Confirmation", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            SaveDriverDataToJson(drivers, driversFilePath);
                            RefreshGridView();
                            ClearDriverInput();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Can't update email", "Error");
                        return;
                    }
                }


            }
            else
            {
                MessageBox.Show("Please select a driver first.", "Message");
            }
        }
        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (driverGridView.SelectedRows.Count > 0)
            {
                int slcRowIndex = driverGridView.SelectedRows[0].Index;
                Driver slcDriver = drivers[slcRowIndex];

                string filePath = "fareinfo.csv";
                List<Tuple<double, double, double>> fareInformation = ReadFareInformation(filePath);

                foreach (Tuple<double, double, double> fareData in fareInformation)
                {
                    double distanceTraveled = fareData.Item1;
                    double traveledUnit = fareData.Item2;
                    double costPerDistance = fareData.Item3;

                    double cheapestFare = CalculateCheapestFare(slcDriver, distanceTraveled, traveledUnit, costPerDistance);

                    FareInfo fare = new FareInfo
                    {
                        DistanceTraveled = distanceTraveled,
                        TraveledUnit = traveledUnit,
                        CostPerDistance = costPerDistance,
                        CheapestFare = cheapestFare,
                        Name = slcDriver.Name,
                        Email = slcDriver.Email
                    };
                    fareHistory.Add(fare);
                    SaveFareHistoryToGridView(fareHistory, fareHistoryPath);
                    RefreshGridView();
                    MessageBox.Show($"Calculated Fare: {cheapestFare}");

                }
            }
            else
            {
                MessageBox.Show("Please select a driver first.", "Message");
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = nameTxt.Text.Trim();
            string surName = surnameTxt.Text.Trim();
            string email = emailTxt.Text.Trim();
            string vehicleType = vehicleTypeTxt.Text.Trim();
            int baseFarePrice;
            int baseFareDistance;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(surName) || string.IsNullOrEmpty(email) ||
                string.IsNullOrEmpty(vehicleType) || !int.TryParse(baseFarePriceTxt.Text, out baseFarePrice) ||
                !int.TryParse(baseFareDistanceTxt.Text, out baseFareDistance))
            {
                MessageBox.Show("Please enter valid data for all fields!", "Error");
                return;
            }

            bool isExits = drivers.Any(r => r.Email == email);
            try
            {
                if (!isExits)
                {
                    Driver driver = new Driver
                    {
                        Name = name,
                        Surname = surName,
                        Email = email,
                        VehicleType = vehicleType,
                        BaseFarePrice = baseFarePrice,
                        BaseFareDistance = baseFareDistance
                    };
                    drivers.Add(driver);
                    SaveDriverDataToJson(drivers, driversFilePath);
                    RefreshGridView();
                    ClearDriverInput();
                }
                else
                {
                    MessageBox.Show("Driver with the same email already exists!", "Error");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while add driver: " + ex.Message, "Error");
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Save Fare Infor History
            SaveFareHistoryToGridView(fareHistory, fareHistoryPath);
        }
        private void btnClearHistory_Click(object sender, EventArgs e)
        {
            if(ClearFareHistory(fareHistoryPath))
            {
                MessageBox.Show("Data cleared successfully!", "Message");
                RefreshGridView();
            }    
        }

        private void RefreshGridView()
        {
            drivers = ReadDriverDataFromJson(driversFilePath);
            driverGridView.DataSource = drivers;
            fareHistory = ReadFareDataFromJson(fareHistoryPath);
            fareGridView.DataSource = fareHistory;
        }
        private void ClearDriverInput()
        {
            nameTxt.Text = "";
            surnameTxt.Text = "";
            emailTxt.Text = "";
            vehicleTypeTxt.Text = "";
            baseFarePriceTxt.Text = "";
            baseFareDistanceTxt.Text = "";
        }

        #region Fare Calculate
        private double CalculateCheapestFare(Driver driver, double distanceTraveled, double traveledUnit, double costPerDistance)
        {
            double cheapestFare = double.MaxValue;

            if (distanceTraveled <= driver.BaseFareDistance)
            {
                cheapestFare = driver.BaseFarePrice;
            }
            else
            {
                double distanceUnits = distanceTraveled - driver.BaseFareDistance;
                double distanceTraveledUnits = distanceUnits / traveledUnit;
                cheapestFare = driver.BaseFarePrice + (distanceTraveledUnits * costPerDistance);
            }

            return cheapestFare;
        }

        private List<Tuple<double, double, double>> ReadFareInformation(string filePath)
        {
            List<Tuple<double, double, double>> fareInformation = new List<Tuple<double, double, double>>();

            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] values = line.Split(',');
                    double distanceTraveled = double.Parse(values[0]);
                    double traveledUnit = double.Parse(values[1]);
                    double costPerDistance = double.Parse(values[2]);

                    fareInformation.Add(Tuple.Create(distanceTraveled, traveledUnit, costPerDistance));
                }
            }

            return fareInformation;
        }

        #endregion

        #region Read & Write Data 
        private List<FareInfo> ReadFareDataFromJson(string filePath)
        {
            try
            {
                string jsonText = File.ReadAllText(filePath);
                fareHistory = JsonConvert.DeserializeObject<List<FareInfo>>(jsonText);
                return fareHistory;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while loading fare info: " + ex.Message, "Error");
            }
            return new List<FareInfo>();

        }
        private void SaveFareHistoryToGridView(List<FareInfo> fareHistory, string filePath)
        {
            try
            {
                string jsonData = JsonConvert.SerializeObject(fareHistory, Formatting.Indented);
                File.WriteAllText(filePath, jsonData);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while save fare info: " + ex.Message, "Error");
            }

        }

        /// <summary>
        /// Read Driver Data From JSON
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private List<Driver> ReadDriverDataFromJson(string filePath)
        {
            try
            {
                string jsonText = File.ReadAllText(filePath);
                drivers = JsonConvert.DeserializeObject<List<Driver>>(jsonText);
                return drivers;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while loading drivers: " + ex.Message, "Error");
            }
            return new List<Driver>();

        }

        /// <summary>
        /// Save Driver 
        /// </summary>
        /// <param name="drivers"></param>
        /// <param name="filePath"></param>
        private void SaveDriverDataToJson(List<Driver> drivers, string filePath)
        {
            try
            {
                string jsonData = JsonConvert.SerializeObject(drivers, Formatting.Indented);
                File.WriteAllText(filePath, jsonData);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while save driver: " + ex.Message, "Error");
            }

        }

        private bool ClearFareHistory(string filePath)
        {
            try
            {
                File.WriteAllText(filePath, "[]");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while clearing data.", "Message");
                return false;
            }
        }
        #endregion

        #region DTO
        public class Driver
        {
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Email { get; set; }
            public string VehicleType { get; set; }
            public int BaseFarePrice { get; set; }
            public int BaseFareDistance { get; set; }
        }

        public class FareInfo
        {
            public double DistanceTraveled { get; set; }
            public double TraveledUnit { get; set; }
            public double CostPerDistance { get; set; }
            public double CheapestFare { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
        }

        #endregion

    }
}